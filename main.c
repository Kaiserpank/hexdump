#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <inttypes.h>

int main(int argc, char **argv)
{
	char *filename;
	FILE *target;
	uint8_t in[16], r;
	char out[81];
	size_t offset, w, range_min, range_max;

	if (argc < 2)
		return -1;

	filename = argv[1];
	target = fopen(filename,"rb");

	if (target == NULL)
		return -2;

	if (argc > 2)
		range_min = strtoul(argv[2], NULL, 10);
	else
		range_min = 0;

	if (argc > 3)
		range_max = strtoul(argv[3], NULL, 10);
	else
		range_max = 0;

	offset = range_min & ~0b1111;
	fseek(target, offset, 0);

	printf("Offset    00 01 02 03 04 05 06 07-08 09 0A 0B 0C 0D 0E 0F  \n");
	while ((r = fread(in, 1, 16, target)) != 0)
	{
		w = 0;
		if (r == 16)
		{
			w = sprintf(out, "%08I64X  %02X %02X %02X %02X %02X %02X %02X %02X"\
					"-%02X %02X %02X %02X %02X %02X %02X %02X ",
					offset,in[0],in[1],in[2],in[3],in[4],in[5],in[6],in[7],
					in[8],in[9],in[10],in[11],in[12],in[13],in[14],in[15]);
		}
		else
		{
			printf("%08I64X  ",offset);

			for (int i = 0; i<16; i++)
			{
				if (i < r)
					sprintf(out+w,"%02X ",in[i]);
				else
					sprintf(out+w,"00 ");

				w += 3;
			}

			out[w-25] = '-';
		}

		out[w] = ' ';
		w++;

		for (int i = 0; i < r; i++)
		{
        	if (in[i] > 31)
				out[w+i] = in[i];
			else
				out[w+i] = '.';
		}

		out[w+r] = 0;
		printf("%s\n",out);
		offset += r;

		if (offset > range_max && range_max > 0)
			break;
	}

	fclose(target);
    return 0;
}